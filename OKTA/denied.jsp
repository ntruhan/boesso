<%@ page import="org.springframework.security.saml.SAMLCredential"%>
<%@ page
	import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@ page import="org.springframework.security.core.context.SecurityContext"%>
<%@ page import="org.springframework.security.core.Authentication"%>
<%@ page import="com.scrippsnetworks.okta.LogAdapter"%>
<%@ page import="com.scrippsnetworks.okta.Log4JLogger"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.util.TimeZone"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href='<c:url value="/css/style.css"/>' media="screen" />
<title>BusinessObjects OKTA SSO Portal - Access Denied</title>
</head>
<body>
	<table id="site-wrapper">
		<tr class="error">
			<td class="text-center"><h1>Access is Denied to SAP BusinessObjects</h1></td>
		</tr>
	</table>
	<table id="site-wrapper"
		style="border: 1px solid #DDD; background: #FCFCFC;">
		<%
		  boolean nullCredentials = true;
		  SAMLCredential credential = null;
		  LogAdapter logger = new Log4JLogger(Log4JLogger.getLogger(this.getClass()));
		  
		  final SimpleDateFormat fmt = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
	      fmt.setTimeZone(TimeZone.getDefault());
		  String dtformat = fmt.format(new Date());
		  
		  try {
		    SecurityContext sc = SecurityContextHolder.getContext();
		    Authentication authentication = sc.getAuthentication();
		    if (authentication == null) {
		      logger.debug("LOGIN ERROR: Invalid OKTA Session login attempt to Business Objects occurred at " + dtformat);
		%>
		<tr>
			<td><h2>OKTA Credentials could not be validated</h2></td>
		</tr>
		<%
		  } else {
		      credential = (SAMLCredential) authentication.getCredentials();
		      String userName = credential.getNameID().getValue();
		      logger.debug("LOGIN ERROR: Attempt for ID: " + userName + " failed at " + dtformat + ".  ID does not exist in Business Objects.");
		%>
		<tr>
			<td><h2>A valid BusinessObjects account could not be found for your ID:&nbsp;<% out.print(userName); %></h2></td>
		</tr>
		<%
		  }
		  }
		  catch (Exception e) {
		    nullCredentials = false;
		  }

		  String bobjEmail = "";
		  if ((getServletContext().getInitParameter("bobjAdminEmail") != null)
		      && (getServletContext().getInitParameter("bobjAdminEmail").length() > 0)) {
		    bobjEmail = getServletContext().getInitParameter("bobjAdminEmail");
		  }

		  if (bobjEmail.length() > 0) {
		%>
		<tr>
			<td><h3>If you are seeing this message and believe you should have access please contact your <a href="mailto:<%out.print(bobjEmail);%>">BusinessObjects Administrator</a></h3></td>
		</tr>
		<%
		  } else {
		%>
		<tr>
			<td><h3>If you are seeing this message and believe you should have access please contact your BusinessObjects Administrator</h3></td>
		</tr>
		<%
		  }
		%>
	</table>
</body>
</html>