<div class="right sidebar" id="sidebar">
    <div class="section">
        <div class="section-content">
            <h2>Useful links</h2>
            <ul class="nice-list">
                <li><a href="http://projects.spring.io/spring-security-saml/">Homepage <span class="quiet">(Spring)</span></a></li>
                <li><a href="http://docs.spring.io/spring-security-saml/docs/">Documentation <span class="quiet">(Spring)</span></a></li>
                <li><a href="https://support.okta.com/help/oktadocumentationpage">Documentation <span class="quiet">(Okta)</span></a></li>
            </ul>
        </div>
        <br>
        <div class="section-content">
            <h2>Spring Support</h2>
            <ul class="nice-list">
                <li><a href="http://stackoverflow.com/questions/tagged/spring-saml">Community <span class="quiet">(Stackoverflow)</span></a></li>
                <li><a href="http://repo.spring.io/list/release/org/springframework/security/extensions/">Releases <span class="quiet">(Spring)</span></a></li>
            </ul>
        </div>        
        <div class="section-content">
            <h2>Okta Support</h2>
            <ul class="nice-list">
                <li><a href="https://support.okta.com/help">Okta Help Center</a></li>
                <li><a href="https://devforum.okta.com/">Okta Developer Forum</a></li>
                <li><a href="https://developer.okta.com/code/java/">Okta Java Integration <span class="quiet">(SAML)</span></a></li>
            </ul>
        </div>
    </div>
</div>
<div class="clearer">&nbsp;</div>