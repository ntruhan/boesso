<div id="footer">
    <div class="left" id="footer-left">
        <p><a href="mailto:vladimir@v7security.com">&copy; 2014 Vladim&#237;r Sch&#228;fer</a></p>
        <p><a href="mailto:vladimir@v7security.com">Modified 2017 Nathan Truhan - Protiviti</a></p>
        <p class="quiet"><a href="http://templates.arcsin.se/">Website template</a> by <a href="http://arcsin.se/">Arcsin</a></p>
        <p class="quiet">This template is licensed under a Creative Commons Attribution 2.5 License: <a href="http://templates.arcsin.se/license/">http://templates.arcsin.se/license/</a></p>
        <div class="clearer">&nbsp;</div>
    </div>
    <div class="right" id="footer-right">
        <p class="large"><a href="#top" class="quiet">Page Top &uarr;</a></p>
    </div>
    <div class="clearer">&nbsp;</div>
</div>