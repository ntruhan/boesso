<%--
/* -*- coding: utf-8 -*-
 * Copyright 2015 Okta, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
--%>

<%@ page import="org.springframework.security.saml.SAMLCredential"%>
<%@ page
	import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@ page
	import="org.springframework.security.core.context.SecurityContext"%>
<%@ page import="org.springframework.security.core.Authentication"%>
<%@ page import="org.opensaml.saml2.core.Attribute"%>
<%@ page import="com.scrippsnetworks.okta.Authenticator"%>
<%@ page import="com.scrippsnetworks.okta.LogAdapter"%>
<%@ page import="com.scrippsnetworks.okta.Log4JLogger"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.util.TimeZone"%>
<%@ page import="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<body style="padding-top: 60px">
	<div class="container">
		<%-- Attribute Pair Table --%>
		<%--Stores Attributes Parsed Form Saml Assertion --%>

		<%
		  boolean authvalid = true;
		  SAMLCredential credential = null;
		  LogAdapter logger = new Log4JLogger(Log4JLogger.getLogger(this.getClass()));

		  final SimpleDateFormat fmt = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
		  fmt.setTimeZone(TimeZone.getDefault());
		  String dtformat = fmt.format(new Date());

		  try {
		    SecurityContext sc = SecurityContextHolder.getContext();
		    Authentication authentication = sc.getAuthentication();
		    credential = (SAMLCredential) authentication.getCredentials();
		    List<Attribute> attributes = credential.getAttributes();
		    pageContext.setAttribute("credential", credential);
		    pageContext.setAttribute("attributes", attributes);
		  }
		  catch (Exception e) {
		    authvalid = false;
		    logger.error("An error occured establishing credentials: " + dtformat + " - " + e.getMessage());
		  }

		  if (authvalid) {
		    String userName = credential.getNameID().getValue();

		    //out.print("<h2>" + userName + "</h2>");

		    //Retrieve username from OKTA
		    boolean bCreateUser = false;

		    if ((getServletContext().getInitParameter("bobjTestID") != null)
		        && (getServletContext().getInitParameter("bobjTestID").length() > 0)) {
		      userName = getServletContext().getInitParameter("bobjTestID");
		    }

		    //Retrieve Trusted Admin and Authentication Key from BOBJ Configuration
		    String sAuthUser = getServletContext().getInitParameter("bobjUser");
		    String sAuthKey = getServletContext().getInitParameter("bobjKey");

		    String BILaunchpad = getServletContext().getInitParameter("bobjBIURL");
		    String sInvalidUserURL = getServletContext().getInitParameter("bobjInvalidUserURL");

		    String sCMS = getServletContext().getInitParameter("bobjCMS");
		    String[] CMS = sCMS.split(",");
		    String[] excludeUsers = { "Administrator", "Guest", "QaaWSServletPrincipal", "SMAdmin" };

		    String sTimeout = getServletContext().getInitParameter("sessionTimeout");
		    int iTimeout = 60;
		    try {
		      iTimeout = Integer.parseInt(sTimeout);
		    }
		    catch (NumberFormatException e) {
		      iTimeout = 60;
		    }

		    String sCreate = getServletContext().getInitParameter("bobjCreateOnLogon");
		    if (sCreate.trim().toLowerCase().contains("true"))
		      bCreateUser = true;

		    Authenticator auth = new Authenticator();
		    try {
		      if (auth.logonUser(userName, sAuthKey, CMS)) {
		        String logonToken = auth.getLogonToken(iTimeout, 2); //Create Token valid for 2 uses, just in case we need a 2nd refresh.  Timeout specified in xml
		        //String logonToken = boe.getDefaultLogonToken();
		        session.setAttribute("token", logonToken);
		        response.sendRedirect(BILaunchpad + "/logon/start.do?ivsLogonToken=" + logonToken);
		      } else { //Invalid Session, re-check user via Admin Account

		        //IUser user = null;
		        boolean bValidUser = false;
		        if (auth.logonUser(sAuthUser, sAuthKey, CMS)) {
		          //Check if User Exists and if-so revalidate aliases
		          //user = auth.getUserByName(userName);

		          if (auth.userExists(userName)) {
		            bValidUser = true;
		            boolean bCreate = auth.createAliases(userName);
		          } else {
		            // Comment Out Create user - User must already exist in system.  may not have aliases
		            //  if (bCreateUser) {
		            //    bValidUser = auth.createUser(userName, "", "");
		            //  }
		          }
		          auth.logoff();
		        }

		        // Logon with validated user profile
		        if (bValidUser) {
		          if (auth.logonUser(userName, sAuthKey, CMS)) {
		            if (auth.isSessionValid()) {
		              String logonToken = auth.getLogonToken(iTimeout, 2); //Create Token valid for 2 uses, just in case we need a 2nd refresh.  Timeout specified in xml

		              //String logonToken = boe.getDefaultLogonToken();
		              session.setAttribute("token", logonToken);
		              response.sendRedirect(BILaunchpad + "/logon/start.do?ivsLogonToken=" + logonToken);
		            }
		          }

		        } else {
		          response.sendRedirect(sInvalidUserURL);
		        }
		      }
		    }
		    catch (Exception e) {
		      logger.error("An authentication error has occured: " + dtformat + " - " + e.getMessage());
		    }

		  }
		%>
	</div>
</body>
</html>
