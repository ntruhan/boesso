package com.scrippsnetworks.okta;

//import org.apache.log4j.Logger;
//import org.slf4j.Logger;

import com.crystaldecisions.enterprise.ocaframework.IManagedService.ManagedExpiredException;
import com.crystaldecisions.sdk.exception.SDKException;
import com.crystaldecisions.sdk.exception.SDKServerException;
import com.crystaldecisions.sdk.framework.CrystalEnterprise;
import com.crystaldecisions.sdk.framework.IEnterpriseSession;
import com.crystaldecisions.sdk.framework.ISessionMgr;
import com.crystaldecisions.sdk.framework.ITrustedPrincipal;
import com.crystaldecisions.sdk.occa.infostore.IInfoStore;
import com.crystaldecisions.sdk.occa.pluginmgr.IPluginInfo;
import com.crystaldecisions.sdk.occa.pluginmgr.IPluginMgr;
import com.crystaldecisions.sdk.occa.security.IUserInfo;

/**
 * Simple wrapper for BOE Enterprise Sessions that provides some convenience
 * methods.
 *
 * @author rwells
 *
 */
public class BOEHelper {

  private interface LogonMethod {
    IEnterpriseSession logon(String nextCMS) throws SDKException;
  }

  private static final String INFOSTORE_SERVICE_KEY = "InfoStore";
  private final LogAdapter logger;

  private IEnterpriseSession entSession;
  private IInfoStore store;
  private String logonToken;


  public BOEHelper() {
    logger = new Log4JLogger(Log4JLogger.getLogger(BOEHelper.class));
  }


  /**
   * Create and return a new logon token using default options
   *
   * @return
   * @throws SDKException
   */
  public String getDefaultLogonToken() throws SDKException {
    return this.getDefaultLogonToken(false);
  }

  /**
   * Gets a Logon Token with default properties valid for 1440 minutes and 10
   * logons. If useExisting is true and this method has already been called
   * once on this object then the same token created earlier will be reused
   * with out having to create another token.
   *
   * @param useExisting
   * @return
   * @throws SDKException
   */
  public String getDefaultLogonToken(final boolean useExisting) throws SDKException {

    // Check for an existing token if reuse is true
    if (useExisting && (this.logonToken != null))
      return this.logonToken;

    // reuse is false or we don't have a token, either way need to create
    // one.
    final String token = this.entSession.getLogonTokenMgr().createLogonToken("", 1440, 10);

    // If reuse is true store the token for future requests.
    if (useExisting) {
      this.logonToken = token;
    }

    return token;
  }

  /**
   * @return an unwrapped InfoStore
   */
  public IInfoStore getInfoStore() {
    return store;
  }

  /**
   * Creates and returns a logon token with provided usage restrictions
   *
   * @param validMinutes
   *            number of minutes the token is valid for.
   * @param validUses
   *            number of times the token can be used to establish a new
   *            sessions
   * @return The token
   * @throws SDKException
   */
  public String getLogonToken(final int validMinutes, final int validUses) throws SDKException {
    // this.entSession.getSerializedSession();
    return this.entSession.getLogonTokenMgr().createLogonToken("", validMinutes, validUses);
  }

  /**
   * @return the specified IPluginInfo
   * @throws SDKException
   */
  public IPluginInfo getPluginInfo(final String pluginName) throws SDKException {
    IPluginInfo result = null;
    final IPluginMgr pluginMgr = store.getPluginMgr();
    result = pluginMgr.getPluginInfo(pluginName);
    return result;
  }

  public IEnterpriseSession getSession() {
    return entSession;
  }

  /**
   * @return Information about the currently logged on user as an IUserInfo
   *         object.
   * @throws SDKException
   */
  public IUserInfo getUserInfo() throws SDKException {
    return entSession.getUserInfo();
  }

  /**
   * Are we connected to a valid Enterprise Session and can we successfully
   * query the CMS Repository.
   *
   * @return boolean
   */
  public boolean isSessionValid() {
    boolean existingSessionValid = false;
    if ((entSession != null) && (store != null)) {
      try {
        // query for empty string is the quickest round trip to the CMS.
        // This should return a CMS generated
        // exception only if the enterprise session is valid
        store.query(""); //$NON-NLS-1$
      }
      catch (final SDKServerException se) {
        // Sever exception expected; existingEntSession is valid.
        existingSessionValid = true;
      }
      catch (final SDKException e) {
        // Sever exception not thrown. existingEntSession is not valid.
        // Just pass on.
        existingSessionValid = false;
      }
      catch (final ManagedExpiredException e) {
        existingSessionValid = false;
      }
    }

    return existingSessionValid;
  }


  /**
   * Logoff and release token if parameter is true.
   *
   * @param releaseToken
   */
  public void logoff(final boolean releaseToken) {
    if (isSessionValid()) {
      if (releaseToken && (this.logonToken != null)) {
        try {
          entSession.getLogonTokenMgr().releaseToken(this.logonToken);
        }
        catch (final SDKException e) {
          // We are logging off and about to terminate the session
          // anyway, so
          // if this fails just move on with life. It probably means
          // the session
          // was already dead so it doesn't matter.
        }
      }
      entSession.logoff();
      entSession = null;
      store = null;
    }
  }

  /**
   * Implements round robin authentication against a set of CMS's either
   * storing an authenticated Enterprise session or throwing an SDKException
   * as to why we could not authenticate.
   *
   * Logic will take the first cms in the list starting at cmsIndex, and
   * attempt to log on to each cms until it finds one that works or has tried
   * them all. Will return either a valid IEnterpriseSession or the last logon
   * exception.
   *
   * We support starting at a non-zero index so that client code can keep
   * track of the last CMS used and attempt to roughly balance load across
   * multiple CMS's in a cluster.
   */
  private int logon(final String[] CMSArray, int cmsIndex, final LogonMethod method) throws SDKException {
    final int size = CMSArray.length;
    int currentIndex = 0;
    final int start = cmsIndex;
    IEnterpriseSession session = null;
    SDKException lastException = null;
    logger.debug("Starting Logon Process with " + size + " cms's to try.");
    while ((currentIndex < size) && (session == null)) {
      // Implementation of circular array indexing
      cmsIndex = (start + currentIndex + size) % size;
      final String nextCMS = CMSArray[cmsIndex].trim();
      logger.debug("Next CMS = " + nextCMS + " at index = " + cmsIndex);
      // Logon
      try {
        session = method.logon(nextCMS);
      }
      catch (final SDKException e) {
        logger.debug("Execption caused by logon " + e.getMessage(), e);
        lastException = e;
        session = null;
      }

      currentIndex++;
    }
    if (session != null) {
      this.setSession(session);
      return cmsIndex;
    } else
      throw lastException;
  }

  /**
   * Logon using Trusted Authentication
   */
  public int logonTrusted(final String username, final String authsecret, final String[] CMSArray, final int cmsIndex)
      throws SDKException {
    return logon(CMSArray, cmsIndex, new LogonMethod() {

      @Override
      public IEnterpriseSession logon(final String nextCMS) throws SDKException {
        final ISessionMgr mgr = CrystalEnterprise.getSessionMgr();
        logger.debug("Creating Trusted Principal");
        final ITrustedPrincipal princ = mgr.createTrustedPrincipal(username, nextCMS, authsecret);
        logger.debug("Logging On Trusted Principal");
        return mgr.logon(princ);
      }
    });
  }

  public void setSession(final IEnterpriseSession session) throws SDKException {
    this.entSession = session;
    this.store = (IInfoStore) entSession.getService(INFOSTORE_SERVICE_KEY);
  }

}
