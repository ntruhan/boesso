package com.scrippsnetworks.okta;

import java.util.Properties;

import com.crystaldecisions.sdk.exception.SDKException;
import com.crystaldecisions.sdk.occa.infostore.IInfoObject;
import com.crystaldecisions.sdk.occa.infostore.IInfoObjects;
import com.crystaldecisions.sdk.occa.infostore.IInfoStore;
import com.crystaldecisions.sdk.plugin.desktop.user.IUser;

/**
 * Commonly used methods for querying the Business Objects Repository.
 *
 * @author rwells
 *
 */
public class QueryHelper {

  private final IInfoStore store;

  // Default logger. Can be overridden with setLogger(LogAdapter)
  private final LogAdapter logger;

  // Boolean to indicate whether to include debug logging
  private boolean debug = true;

  public QueryHelper(final BOEHelper boe, final boolean dbg) {
    this(boe, new Log4JLogger(Log4JLogger.getLogger(QueryHelper.class)), dbg);
  }

  private QueryHelper(final BOEHelper boe, final LogAdapter newLogAdapter, final boolean dbg) {
    this(boe.getInfoStore(), newLogAdapter, dbg);
  }

  private QueryHelper(final IInfoStore store, final LogAdapter newLogAdapter, final boolean dbg) {
    this.store = store;
    this.logger = newLogAdapter;
    this.debug = dbg;

    try {
      final Properties config = new Properties();
      config.load(getClass().getResourceAsStream("/QueryHelper.properties"));
    }
    catch (final Exception e) {
      logger.error("Could not retrieve MAX_BATCH from Properties file, assign default value of 1000");
    }

  }

  /**
   * Executes a plain sql style query with no limits on returned results or
   * pagination. Be sure you know the scale of the query being executed so as
   * not to overwhelm the system.
   *
   * @param query
   *          The query
   * @return The results
   * @throws SDKException
   */
  private IInfoObjects executeRawQuery(final String query) throws SDKException {
    // logger.debug(query);
    if (debug) {
      logger.debug(query);
    }
    return store.query(query);
  }

  /**
   * Helper method to ensure one and only one object is returned.
   */
  private IInfoObject getObject(final IInfoObjects objs) throws SDKException {
    if (objs.size() > 0)
      return (IInfoObject) objs.get(0);
    else
      return null;
  }

  /**
   * Retrieves an Object by SI_NAME and SI_KIND. Works for INFO, SYSTEM, and
   * APP objects. Important to note that Name and Kind is not necessarily a
   * unique key. This method will return the first object found if multiple
   * are found.
   *
   * @param name
   *          SI_NAME of object
   * @param kind
   *          SI_KIND of object
   * @return InfoObject with all columns
   * @throws SDKException
   */
  private IInfoObject getObjectByName(final String name, final String kind) throws SDKException {
    return getObjectByName(name, kind, true);
  }

  /**
   * Retrieves an Object by SI_NAME and SI_KIND. Works for INFO, SYSTEM, and
   * APP objects. Important to note that Name and Kind is not necessarily a
   * unique key. This method will return the first object found if multiple
   * are found. This version allows the inclusion of instances whereas the
   * default version does not.
   *
   * @param name
   *          SI_NAME of object
   * @param kind
   *          SI_KIND of object
   * @return InfoObject with all columns
   * @throws SDKException
   */
  private IInfoObject getObjectByName(final String name, final String kind, final boolean noInstances)
      throws SDKException {
    String query = "SELECT * FROM CI_INFOOBJECTS, CI_SYSTEMOBJECTS, CI_APPOBJECTS WHERE SI_NAME = '" + name
        + "' AND SI_KIND = '" + kind + "'";
    if (noInstances) {
      query += " AND SI_INSTANCE=0";
    }
    return getObject(executeRawQuery(query));
  }



  /** @see #getObjectByName(String, String) */
  public IUser getUserByName(final String name) throws SDKException {
    return (IUser) getObjectByName(name, "User");
  }


}
