package com.scrippsnetworks.okta;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Iterator;

import com.crystaldecisions.sdk.exception.SDKException;
import com.crystaldecisions.sdk.plugin.desktop.user.IUser;
import com.crystaldecisions.sdk.plugin.desktop.user.IUserAlias;
import com.crystaldecisions.sdk.plugin.desktop.user.IUserAliases;

@SuppressWarnings("rawtypes")
public class AliasHelper {
  private static final String secEnterprise = "secEnterprise";
  private static final String secLDAP = "secLDAP";

  /**
   * Generic Alias add method used by other add Methods.
   *
   * @param user
   *            user to add to
   * @param userName
   *            prepared name with authtype prefix applied.
   * @throws SDKException
   */
  private static void addAlias(final IUser user, final String userName) throws SDKException {
    final IUserAlias newAlias = user.getAliases().addNew(userName, false);
    try {
      user.save();
    }
    catch (final SDKException e) {
      // If the save fails it most likely indicates a problem with the
      // alias
      // Because we are going to add other aliases to this object later,
      // we cant
      // leave the bad alias dangling around.
      user.getAliases().remove(newAlias);
      throw e;
    }
  }

  /**
   * Adds an Enterprise Alias to a user. Invokes save immediately.
   *
   * @param user
   *            to add alias to
   * @param enterpriseUserId
   *            user id for new alias
   * @param password
   *            password to set on user object to go with enterprise alias
   * @throws SDKException
   */
  public static void addEnterpriseAlias(final IUser user, final String enterpriseUserId, final String password)
      throws SDKException {
    final String userName = secEnterprise + ":" + enterpriseUserId;
    addAlias(user, userName);
    user.setNewPassword(password);
    user.save();
  }

  /**
   * Attempts to add an LDAP Alias to a user. Invokes Save immediately as it's
   * the only way to generate exceptions for unmapped groups or pre-existing
   * aliases.
   *
   * @param user
   *            The user to add alias to.
   * @param ldapUserId
   *            User ID of LDAP alias
   * @throws SDKException
   *             if not a member of a mapped group or pre-existing alias
   *             exists.
   */
  public static void addLDAPAlias(final IUser user, final String ldapUserId) throws SDKException {
    final String userName = secLDAP + ":" + ldapUserId;
    addAlias(user, userName);
  }

  /**
   * Generate Random Password in Hex Encoded Format
   *
   * @param byteLength
   *            integer the length of the hex token to return
   * @return String Password Token
   */
  public static String generateRandomHexToken(int byteLength) {
    final SecureRandom secureRandom = new SecureRandom();
    final byte[] token = new byte[byteLength];
    secureRandom.nextBytes(token);
    return new BigInteger(token).toString(16); //hex encoding
  }


  /**
   * Determines if a user has a pre-existing Alias
   *
   * @param user
   *            the user to check
   * @param aliasType
   *            the type of alias to look for
   * @return true if alias of the specified type found, false otherwise
   * @throws SDKException
   */
  public static boolean userHasAlias(final IUser user, final String aliasType) throws SDKException {
    final IUserAliases aliases = user.getAliases();
    for (final Iterator i = aliases.iterator(); i.hasNext();) {
      final IUserAlias a = (IUserAlias) i.next();
      // There is a strange slim chance that getAuthentication will throw
      // a null pointer here.
      // if it does, it most likely doesn't match our desired alias type
      // so treat it as false.
      try {
        if (a.getAuthentication().equals(aliasType))
          return true;
      }
      catch (final NullPointerException ne) {
        // do nothing, and act as if not equal.
      }
    }
    return false;
  }


}
