package com.scrippsnetworks.okta;

//import org.apache.log4j.Logger;
import org.slf4j.Logger;

public class Log4JLogger implements LogAdapter {

  /**
   * Shorthand for <code>getLogger(clazz.getName())</code>.
   *
   * @param clazz The name of <code>clazz</code> will be used as the
   * name of the logger to retrieve.  See {@link #getLogger(String)}
   * for more detailed information.
   *
   * Code Utilized from apache log4j.Logger
   */
  static public Logger getLogger(@SuppressWarnings("rawtypes") Class clazz) {
    return org.slf4j.LoggerFactory.getLogger(clazz.getName());
  }

  private final Logger logger;

  public Log4JLogger(final Logger log) {
    this.logger = log;
  }

  @Override
  public void debug(final Object paramObject) {
    logger.debug((String) paramObject);
  }

  @Override
  public void debug(final Object paramObject, final Throwable paramThrowable) {
    logger.debug((String) paramObject, paramThrowable);
  }

  @Override
  public void error(final Object paramObject) {
    logger.error((String) paramObject);
  }

  @Override
  public void error(final Object paramObject, final Throwable paramThrowable) {
    logger.error((String) paramObject, paramThrowable);
  }

  @Override
  public void info(final Object paramObject) {
    logger.info((String) paramObject);
  }

  @Override
  public void info(final Object paramObject, final Throwable paramThrowable) {
    logger.info((String) paramObject, paramThrowable);
  }

  @Override
  public void trace(final Object paramObject) {
    logger.trace((String) paramObject);
  }

  @Override
  public void trace(final Object paramObject, final Throwable paramThrowable) {
    logger.trace((String) paramObject, paramThrowable);
  }

  @Override
  public void warn(final Object paramObject) {
    logger.warn((String) paramObject);
  }

  @Override
  public void warn(final Object paramObject, final Throwable paramThrowable) {
    logger.warn((String) paramObject, paramThrowable);
  }

}
