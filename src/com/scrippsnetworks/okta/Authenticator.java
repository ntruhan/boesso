package com.scrippsnetworks.okta;

import com.crystaldecisions.sdk.exception.SDKException;
import com.crystaldecisions.sdk.occa.infostore.IInfoObjects;
import com.crystaldecisions.sdk.occa.pluginmgr.IPluginInfo;
import com.crystaldecisions.sdk.plugin.desktop.user.IUser;

public class Authenticator {

  BOEHelper boe;

  QueryHelper qh;

  public Authenticator() {
    boe = new BOEHelper();
  }

  /**
   * Checks if User Object has both an Enterprise and LDAP alias
   * If not, it attemps to create both of these aliases
   *
   * @param user
   *          IUSER user object
   * @return boolean aliases are created
   */
  public boolean createAliases(final IUser user) {
    boolean bRet = false;
    try {
      if (!AliasHelper.userHasAlias(user, "secEnterprise")) {
        final String sPasswd = AliasHelper.generateRandomHexToken(12);
        AliasHelper.addEnterpriseAlias(user, user.getTitle(), sPasswd);
      }

      if (!AliasHelper.userHasAlias(user, "secLDAP")) {
        AliasHelper.addLDAPAlias(user, user.getTitle());
      }
      bRet = true;
    }
    catch (final SDKException e) {
      bRet = false;
    }

    return bRet;
  }


  /**
   * Checks if User Name has both an Enterprise and LDAP alias
   * If not, it attemps to create both of these aliases
   * Wrapper call to get user object and pass to createAlias(IUser)
   *
   * @param user
   *          String user name
   * @return boolean aliases are created
   */
  public boolean createAliases(final String userName) {
    final IUser cUser = getUserByName(userName);
    return createAliases(cUser);
  }

  /**
   * Creates a new user object in the system with a specified username, name and description
   * When new user created, aliases are also created
   *
   *
   * @param userName
   *          String user login
   * @param fullName
   *          String user full name
   * @param description
   *          String user description
   * @return boolean status of user creation created
   */
  public boolean createUser(final String userName, final String fullName, final String description) {
    boolean bRet = false;
    IPluginInfo newBoUserPlugin;
    try {
      newBoUserPlugin = boe.getPluginInfo("CrystalEnterprise.User");

      final IInfoObjects users = boe.getInfoStore().newInfoObjectCollection();
      final IUser newUser = (IUser) users.add(newBoUserPlugin);
      newUser.setTitle(userName);
      newUser.setNewPassword(AliasHelper.generateRandomHexToken(12));
      newUser.setFullName(""); // Get Full Name from OKTA
      newUser.setDescription("");
      boe.getInfoStore().commit(users);
      bRet = true;
    }
    catch (final SDKException e) {
      bRet = false;
    }

    //If User created successfully, create Enterprise and LDAP aliases
    if (bRet) bRet = createAliases(userName);

    return bRet;
  }

  /**
   * Get BusinessObjects Logon Token to use with ivsLogonToken parameter
   *
   *
   * @param validMinutes
   *          integer how long session is valid for
   * @param validuses
   *          integer how many uses this token is valid for before expiring
   * @return string token to use
   */
  public String getLogonToken(final int validMinutes, final int validUses) {
    String logonToken = "";
    try {
      logonToken = boe.getLogonToken(validMinutes, validUses);
    }
    catch (final SDKException e) {

    }
    return logonToken;
  }

  /**
   * Get BusinessObjects IUser Object for user by name
   *
   * @param userName
   *          String user name to retrieve
   * @return IUser object representing user
   */
  public IUser getUserByName(final String userName) {
    IUser retUser = null;
    // Check if User Exists and if-so revalidate aliases
    try {
      retUser = qh.getUserByName(userName);
    }
    catch (final SDKException e) {
    }
    return retUser;
  }

  /**
   * Is BusinessObjects Logon Session Valid
   *
   * @return boolean session valid
   */
  public boolean isSessionValid() {
    return boe.isSessionValid();
  }

  /**
   * Logoff BusinessObjects Session
   *
   */
  public void logoff() {
    if (qh != null) {
      qh = null;
    }
    boe.logoff(true);
  }

  /**
   * Create a new Business Objects Trusted logon session
   *
   * @param userName
   *          String user name to retrieve
   * @param AuthKey
   *          String Trusted Auth Key
   * @param CMS
   *          String CMS to log into
   * @return boolean user logged in
   */
  @SuppressWarnings("unused")
  public boolean logonUser(final String userName, final String AuthKey, final String[] CMS) {
    boolean bLogon = false;
    try {
      // Attempt to login as User being sent.
      if (boe == null) {
        boe = new BOEHelper();
      }
      final int iLogon = boe.logonTrusted(userName, AuthKey, CMS, 0);
      qh = new QueryHelper(boe, false);
      bLogon = boe.isSessionValid();
    }
    catch (final SDKException e) { // Catch Logon Failure
    }
    return bLogon;
  }

  public boolean userExists(final String userName) {
    IUser retUser = null;
    boolean bRet = false;
    // Check if User Exists and if-so revalidate aliases
    try {
      retUser = qh.getUserByName(userName);
      if (retUser != null) {
        bRet = true;
      }
    }
    catch (final SDKException e) {
    }
    return bRet;
  }

}